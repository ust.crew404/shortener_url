<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct(){
		parent::__construct();
        $this->load->database();
		$this->load->model('crud');
		$this->load->helper('url');
	}

	public function index()
	{
		$data['list'] = $this->crud->tampil_data('short_url')->result();
		$this->load->view('front/plugin/header', $data);
		$this->load->view('front/index', $data);
		$this->load->view('front/plugin/footer', $data);
	}
	public function u($code)
	{
		if(!empty($code)){
			$ip = $this->crud->info_client_ip_getenv();//cek ip
			$acuan = array('code' => $code,
							'status' => 1
							);
			$cek = $this->crud->where_num('short_url', $acuan);
			if($cek == 1){
				$url = $this->crud->user_where('short_url', $acuan);
				$url_short = $url[0]['url_short'];
				$url_redirect = $url[0]['url_asli'];

				$data = array(
								'url' => $url_short,
								'ip' => $ip
							 );
				$this->crud->Insert('statistic',$data);
				redirect($url_redirect);
			}else{
				$this->load->view('front/plugin/header');
				$this->load->view('front/error');
				$this->load->view('front/plugin/footer');
			}
		}
	}
	public function login()
	{
		$this->load->view('front/plugin/header');
		$this->load->view('front/login');
		$this->load->view('front/plugin/footer');
	}
	public function login_aksi()
	{
		$username = $this->input->post('user');
		$password = md5($this->input->post('pass'));
		$data = array(
					'npp' => $username,
					'password' => $password
			);
		$cek = $this->crud->Get_user('tbl_user', $data)->num_rows();

		if($cek == 1){
			$cek_level = $this->crud->user_where('tbl_user', $data);
			$level = $cek_level[0]["level"];
			if ($level == 1) {
				# code...
				$data_session = array(
				'nama' => $username,
				'status' => "login"
				);
 
				$this->session->set_userdata($data_session);

				$last_login = array('last_login' => date("Y-m-d H:i:s"));
				$this->crud->update_data($data,$last_login,'tbl_user');
				redirect(base_url("admin/dashboard"));
			}elseif ($level == 0) {
				# code...
				$data_session = array(
				'nama' => $username,
				'status' => "login"
				);
 				
				$this->session->set_userdata($data_session);

				$last_login = array('last_login' => date("Y-m-d H:i:s"));
				$this->crud->update_data($data,$last_login,'tbl_user');

				redirect(base_url("user/dashboard"));
			}
		}else{
				$this->session->set_flashdata('fail', 'NIM / NPP atau Password Salah.');
				redirect(base_url().'home/login');
			}
	}
}
