<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {


	function __construct(){
		parent::__construct();
        $this->load->database();
		$this->load->helper('text');
		$this->load->model('crud');
		$this->load->helper('url');
		if($this->session->userdata('status') != "login"){
			redirect(base_url("/login"));
		}
	}
	public function index()
	{
    redirect(base_url().'user/dashboard');
	}
  
  public function dashboard()
  {
    $this->load->view('back_user/plugin/header');
    $this->load->view('back_user/index');
    $this->load->view('back_user/plugin/footer');
  }
  public function listUrl()
  {
  	$acuan = array('username' => $this->session->userdata('nama'));
    $data['list_url'] = $this->crud->Get_where('short_url', $acuan);
    // var_dump($data);
    $this->load->view('back_user/plugin/header', $data);
    $this->load->view('back_user/listUrl', $data);
    $this->load->view('back_user/plugin/footer', $data);
  }
  public function add_url()
  {
      $url_awal = $this->input->post('url'); 
      $cekslug = $this->input->post('slug');
      $description = $this->input->post('desc');
      $url = 'localhost/';

      if(!empty($cekslug))
      {
        $slug = str_replace(' ', '', $cekslug);
        $data = array(
                    'slug' => $slug
          );
        $cek = $this->crud->where_num('short_url', $data);
        if($cek > 0)
        {
          $this->session->set_flashdata('input', 'slug sudah ada.');
          redirect(base_url().'user/listUrl');
        }else{
          $short_url = $url.$slug;
          $data = array(
                  'username' => $this->session->userdata('nama'),
                  'url_asli' => $url_awal,
                  'url_short' => $short_url,
                  'code' => $slug,
                  'slug' => $slug,
                  'description' => $description,
                  'status' => 1
            );
          $this->crud->Insert('short_url', $data);
          $this->session->set_flashdata('sukses', 'shortener url success.');
          redirect(base_url().'user/listUrl');
        }
      }else{
        $code = $this->crud->acak_code(5);
        $acuan = array('code' => $code);
        $cek = $this->crud->where_num('short_url', $acuan);
        $url_awal = $this->input->post('url');
        $description = $this->input->post('desc');
        if($cek > 0)
        {
          $code = $this->crud->acak_code(6);
          $short_url = $url.$code;
          $data = array(
                  'username' => $this->session->userdata('nama'),
                  'url_asli' => $url_awal,
                  'url_short' => $short_url,
                  'code' => $code,
                  'slug' => '-',
                  'description' => $description,
                  'status' => 1
            );
          $this->crud->Insert('short_url', $data);
          $this->session->set_flashdata('sukses', 'shortener url success.');
          redirect(base_url().'user/listUrl');
        }else{
          $short_url = $url.$code;
          $data = array(
                  'username' => $this->session->userdata('nama'),
                  'url_asli' => $url_awal,
                  'url_short' => $short_url,
                  'code' => $code,
                  'slug' => '-',
                  'description' => $description,
                  'status' => 1
            );
          $this->crud->Insert('short_url', $data);
          $this->session->set_flashdata('sukses', 'shortener url success.');
          redirect(base_url().'user/listUrl');
        }
      }
  }
  public function edit_url($id)
  {
    if(!empty($id)){
      $where = array('id' => $id);
      $data['get_where'] = $this->crud->Get_where('short_url', $where);
      $this->load->view('back_user/plugin/header', $data);
      $this->load->view('back_user/edit_url', $data);
      $this->load->view('back_user/plugin/footer', $data);
    }else{
      redirect(base_url().'user/listUrl');
    }
  }
  public function edit_act(){
    $id = $this->input->post('id');
    $url_awal = $this->input->post('url');
    $slug = $this->input->post('slug');
    $description = $this->input->post('desc');
    $status = $this->input->post('status');
    $url = 'localhost/';
    $short_url = $url.$slug;

    $where = array('id' => $id);
    $data = array(
                  'url_asli' => $url_awal,
                  'url_short' => $short_url,
                  'code' => $slug,
                  'slug' => $slug,
                  'description' => $description,
                  'status' => $status 
                );
    $this->crud->update_data($where,$data,'short_url');
    redirect(base_url().'user/listUrl');
  }
  public function delete_url($id)
  {
    $where = array('id' => $id);
    $this->crud->hapus_data($where,'short_url');
    redirect(base_url().'user/listUrl');
  }
  public function statistic(){
    // $total_klik = $this->crud->hitung('statistic', )
    $data['listStatistic'] = $this->crud->tampil_data('statistic')->result();
    $this->load->view('back_user/plugin/header', $data);
    $this->load->view('back_user/statistic', $data);
    $this->load->view('back_user/plugin/footer', $data);
  }
  function logout(){
		$this->session->sess_destroy();
		redirect(base_url());
	}
}