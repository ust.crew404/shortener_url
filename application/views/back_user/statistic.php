    <div class="page">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="javascript:void(0);">Statistic</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-align-justify"></i>
            </button>
        </nav>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-6 col-sm-12">
                    <div class="card widget_2 big_icon traffic">
                        <div class="body">
                          <div class="table-responsive">
                            <div id="DataTables_Table_1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                                    <table class="table table-bordered table-striped table-hover dataTable js-exportable" id="DataTables_Table_1" role="grid" aria-describedby="DataTables_Table_1_info">
                                        <thead>
                                            <tr role="row">
                                                <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="No: activate to sort column descending" style="width: 100px;">No</th>
                                                <th class="sorting" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" aria-label="URL: activate to sort column ascending" style="width: 397px;">URL</th>
                                                <th class="sorting" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" aria-label="Short: activate to sort column ascending" style="width: 187px;">IP ADDRESS</th>
                                                <!-- <th class="sorting" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" aria-label="Slug: activate to sort column ascending" style="width: 201px;">TOTAL CLICK</th> -->
                                                <th class="sorting" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" aria-label="Description: activate to sort column ascending" style="width: 281px;">DATE TIME</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th rowspan="1" colspan="1">No</th>
                                                <th rowspan="1" colspan="1">URL</th>
                                                <th rowspan="1" colspan="1">IP ADDRESS</th>
                                                <!-- <th rowspan="1" colspan="1">TOTAL CLICK</th> -->
                                                <th rowspan="1" colspan="1">DATE TIME</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        <?php 
                                        $no=1;
                                        foreach($listStatistic as $ls){
                                            ?>     
                                            <tr role="row" class="odd">
                                                <td class="sorting_1"><?php echo $no++?></td>
                                                <td><a href="<?php echo $ls->url; ?>"><?php echo $ls->url ?></a></td>
                                                <td><a href="<?php echo $ls->ip; ?>"><?php echo $ls->ip ?></a></td>
                                                <td><?php echo $ls->datetime ?></td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>    
                        </div>
                    </div>
                </div>
            </div>    
        </div>

