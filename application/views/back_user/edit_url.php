    <div class="page">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="javascript:void(0);">List URLs</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-align-justify"></i>
            </button>
        </nav>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-6 col-sm-12">
                    <div class="card widget_2 big_icon traffic">
                        <div class="body">
                            <div class="col-lg-2">
                                <a class="btn btn-block btn-primary active" href="<?php echo base_url().'user/listUrl';?>">Back to List</a>
                            </div>
                            <hr>
                            <form action="<?php echo base_url().'user/edit_act'; ?>" method="post">
                                <?php foreach($get_where as $gw){?>
                                            <input type="text" class="form-control" hidden="" value="<?php echo $gw->id ?>" aria-label="URL" aria-describedby="basic-addon1" name="id">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">URL</span>
                                                    </div>
                                                    <input type="text" class="form-control" value="<?php echo $gw->url_asli ?>" aria-label="URL" aria-describedby="basic-addon1" name="url" required="">
                                                </div>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Slug</span>
                                                    </div>
                                                    <?php $slug = $gw->url_short;
                                                          $slug = substr($slug, 17);
                                                    ?>
                                                    <input type="text" class="form-control" value="<?php echo $slug; ?>" aria-label="slug" name="slug" aria-describedby="basic-addon1" maxlength="10">
                                                </div>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Description</span>
                                                    </div>
                                                    <input type="text" class="form-control" value="<?php echo $gw->description ?>" aria-label="Description" name="desc" aria-describedby="basic-addon1" required="">
                                                </div>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label class="input-group-text" for="inputGroupSelect01">Status</label>
                                                    </div>
                                                    <select name="status" class="custom-select" id="inputGroupSelect01">
                                                        <option selected="" value="<?php echo $gw->status ?>"><?php 
                                                             if($gw->status == 1){
                                                         echo "Active";
                                                       }elseif ($gw->status == 0) {
                                                           # code...
                                                          echo "Inactive";
                                                       } 
                                                        ?></option>
                                                        <option value="1">Active</option>
                                                        <option value="0">Inactive</option>
                                                    </select>
                                                </div>
                                                <div class="input-group mb-3">
                                                    <input type="submit" class="btn btn-block btn-primary active" value="Edit">
                                                </div>
                                        <?php }?>
                                        </form>
                </div>
            </div>
        </div>
    </div>    
</div>

