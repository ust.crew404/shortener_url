<!-- Core -->
<script src="https://kit.fontawesome.com/cccb025974.js" crossorigin="anonymous"></script>
<script src="<?php echo base_url().'/assets/backend/bundles/libscripts.bundle.js'; ?>"></script>
<script src="<?php echo base_url().'/assets/backend/bundles/vendorscripts.bundle.js'; ?>"></script>

<script src="<?php echo base_url().'/assets/backend/bundles/c3.bundle.js'; ?>"></script>
<script src="<?php echo base_url().'/assets/backend/bundles/jvectormap.bundle.js'; ?>"></script> <!-- JVectorMap Plugin Js -->

<script src="<?php echo base_url().'/assets/backend/js/theme.js'; ?>"></script>
<script src="<?php echo base_url().'/assets/backend/js/pages/index.js'; ?>"></script>
<script src="<?php echo base_url().'/assets/backend/js/pages/todo-js.js'; ?>"></script>

<!-- Jquery DataTable Plugin Js --> 
<script src="<?php echo base_url().'/assets/backend/bundles/datatablescripts.bundle.js'; ?>"></script>
<script src="<?php echo base_url().'/assets/backend/vendor/jquery-datatable/buttons/dataTables.buttons.min.js'; ?>"></script>
<script src="<?php echo base_url().'/assets/backend/vendor/jquery-datatable/buttons/buttons.bootstrap4.min.js'; ?>"></script>
<script src="<?php echo base_url().'/assets/backend/vendor/jquery-datatable/buttons/buttons.colVis.min.js'; ?>"></script>
<script src="<?php echo base_url().'/assets/backend/vendor/jquery-datatable/buttons/buttons.flash.min.js'; ?>"></script>
<script src="<?php echo base_url().'/assets/backend/vendor/jquery-datatable/buttons/buttons.html5.min.js'; ?>"></script>
<script src="<?php echo base_url().'/assets/backend/vendor/jquery-datatable/buttons/buttons.print.min.js'; ?>"></script>

<script src="<?php echo base_url().'/assets/backend/js/pages/tables/jquery-datatable.js'; ?>"></script>

</body>
</html>