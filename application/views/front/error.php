        
        <div class="back-to-home rounded d-none d-sm-block">
            <a href="<?php echo base_url(); ?>" class="text-white rounded d-inline-block text-center"><i class="mdi mdi-home"></i></a>
        </div>

        <!-- ERROR PAGE -->
        <section class="bg-home">
            <div class="home-center">
                <div class="home-desc-center">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-8 col-md-12 text-center">
                                <img src="<?php echo base_url().'assets/frontend/images/logo/404.png'; ?>" class="img-fluid" alt="">
                                <div class="text-uppercase mt-4 display-3">Oh ! no</div>
                                <div class="text-capitalize text-dark mb-4 error-page">Page Not Found</div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-center">  
                                <a href="<?php echo base_url(); ?>" class="btn btn-outline-primary mt-4">Go Back</a>
                                <a href="<?php echo base_url(); ?>" class="btn btn-primary mt-4 ml-2">Go To Home</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>