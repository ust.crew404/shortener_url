         <!-- Navbar STart -->
        <header id="topnav" class="defaultscroll sticky">
            <div class="container">
                <!-- Logo container-->
                <div>
                    <a class="logo" href="<?php echo base_url();?>"><span class="text-primary">UDINUS</span></a>
                </div>                 
                <div class="buy-button">
                    <a href="<?php echo base_url().'home/login'; ?>" class="btn btn-primary">LOGIN</a>
                </div><!--end login button-->
                <!-- End Logo container-->
                <div class="menu-extras">
                    <div class="menu-item">
                        <!-- Mobile menu toggle-->
                        <!-- <a class="navbar-toggle">
                            <div class="lines">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </a> -->
                        <!-- End mobile menu toggle-->
                    </div>
                </div>
        
                <div id="navigation">
                    <!-- Navigation Menu-->   
                    <ul class="navigation-menu">
                    </ul><!--end navigation menu-->
                </div><!--end navigation-->
            </div><!--end container-->
        </header><!--end header-->
        <!-- Navbar End -->
        
        <!-- Hero Start -->
        <section class="bg-half-170" id="home">
            <div class="home-center">
                <div class="home-desc-center">
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-lg-7 col-md-7">
                                <div class="title-heading mt-4">
                                    <h1 class="heading mb-3">Shortener URL<br><span class="text-primary">Universitas Dian Nuswantoro</span> </h1>
                                    <p class="para-desc text-muted">Shortener URLs by Universitas Dian Nuswantoro Semarang.</p>
                                    <div class="mt-4 pt-2">
                                        <a href="#list" class="btn btn-primary mt-2 mr-2">List Shortened URL</a>
                                    </div>
                                </div>
                            </div><!--end col-->

                            <div class="col-lg-5 col-md-5 mt-4 pt-2 mt-sm-0 pt-sm-0">
                                <img src="<?php echo base_url().'assets/frontend/images/illustrator/Startup_SVG.svg'; ?>" alt="">
                            </div><!--end col-->
                        </div><!--end row-->
                    </div><!--end container--> 
                </div><!--end home desc center-->
            </div><!--end home center-->
        </section><!--end section-->
        <!-- Hero End -->

        <!-- Partners start -->
        
        <!-- Partners End -->

        <!-- How It Work Start -->
        <section class="section bg-light border-bottom">
            <div class="container" id="list">
                <div class="row justify-content-center">
                    <div class="col-12 text-center">
                        <div class="section-title mb-60">
                            <h4 class="main-title mb-4">List Shortened URL</h4>
                            <table id="example" class="table table-bordered" width="100%">
                            	 <thead>
						            <tr>
						                <th>No</th>
						                <th>URL</th>
						                <th>Short URL</th>
						                <th>Description</th>
						                <th>Status</th>
						            </tr>
						        </thead>
						        <tbody>
                                <?php $no = 1; foreach($list as $l){?>
						            <tr>
						                <td><?php echo $no++;?></td>
						                <td><a href="<?php echo $l->url_asli ?>"><?php echo $l->url_asli ?></a></td>
						                <td><a href="<?php echo $l->url_short ?>"><?php echo $l->url_short ?></a></td>
						                <td><?php echo $l->description ?></td>
						                <td>
                                        <?php if($l->status == 1){
                                            echo "<a class='btn btn-success btn-sm'> Active </a>";
                                            }else{
                                                echo "<a class='btn btn-danger btn-sm'> Inactive </a>";
                                                }?>
                                        </td>
						            </tr>
                                <?php } ?>
                                   </tbody>
						          <tfoot>
							            <tr>
							                <th>No</th>
							                <th>URL</th>
							                <th>Short URL</th>
							                <th>Description</th>
							                <th>Status</th>
							            </tr>
							        </tfoot>
                            </table>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->
            </div><!--end container-->
        </section><!--end section-->
        <!-- How It Work End -->
        <!-- Pricing End -->

        <!-- FAQ n Contact Start -->
        <section class="section bg-light">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-12">
                        <div class="faq-container">
                            <h4 class="question"><i class="mdi mdi-help-circle text-primary mr-2 h4"></i> How our <span class="text-primary">Landrick</span> work ?</h4>
                            <p class="answer text-muted ml-lg-4 pl-lg-3 mb-0">Due to its widespread use as filler text for layouts, non-readability is of great importance: human perception is tuned to recognize certain patterns and repetitions in texts.</p>
                        </div>
                    </div><!--end col-->
                    
                    <div class="col-md-6 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                        <div class="faq-container">
                            <h4 class="question"><i class="mdi mdi-help-circle text-primary mr-2 h4"></i> What is the main process open account ?</h4>
                            <p class="answer text-muted ml-lg-4 pl-lg-3 mb-0">If the distribution of letters and 'words' is random, the reader will not be distracted from making a neutral judgement on the visual impact</p>
                        </div>
                    </div><!--end col-->
                    
                    <div class="col-md-6 col-12 mt-4 pt-2">
                        <div class="faq-container">
                            <h4 class="question"><i class="mdi mdi-help-circle text-primary mr-2 h4"></i> How to make unlimited data entry ?</h4>
                            <p class="answer text-muted ml-lg-4 pl-lg-3 mb-0">Furthermore, it is advantageous when the dummy text is relatively realistic so that the layout impression of the final publication is not compromised.</p>
                        </div>
                    </div><!--end col-->
                    
                    <div class="col-md-6 col-12 mt-4 pt-2">
                        <div class="faq-container">
                            <h4 class="question"><i class="mdi mdi-help-circle text-primary mr-2 h4"></i> Is <span class="text-primary">Landrick</span> safer to use with my account ?</h4>
                            <p class="answer text-muted ml-lg-4 pl-lg-3 mb-0">The most well-known dummy text is the 'Lorem Ipsum', which is said to have originated in the 16th century. Lorem Ipsum is composed in a pseudo-Latin language which more or less corresponds to 'proper' Latin.</p>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->

                <div class="row mt-md-5 pt-md-3 mt-4 pt-2 mt-sm-0 pt-sm-0 justify-content-center">
                    <div class="col-12 text-center">
                        <div class="section-title">
                            <h4 class="main-title mb-4">Have Question ? Get in touch!</h4>
                            <p class="text-muted para-desc mx-auto">Start working with <span class="text-primary font-weight-bold">Landrick</span> that can provide everything you need to generate awareness, drive traffic, connect.</p>
                            <a href="javascript:void(0)" class="btn btn-primary mt-4">Contact us <i class="mdi mdi-arrow-right"></i></a>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->
            </div><!--end container-->
            <div class="container-fluid">
                <div class="row">
                    <div class="home-shape-bottom">
                        <img src="<?php echo base_url().'assets/frontend/images/shapes/shape-dark.png'; ?>" alt="" class="img-fluid mx-auto d-block">
                    </div> 
                </div><!--end row-->
            </div> <!-- END CONTAINER -->
        </section><!--end section-->
        <!-- FAQ n Contact End -->

        <!-- Footer Start -->
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 col-12 mb-0 mb-md-4 pb-0 pb-md-2">
                    	<img src="<?php echo base_url().'assets/frontend/images/logo/udinus.png'; ?>" style="width: 100px;">
                        <!-- <a class="logo-footer" href="#">Landrick<span class="text-primary">.</span></a> -->
                        <p class="mt-4"><font style="color: white; font-size: 18px;">Universitas Dian Nuswantoro</font></p>
                        <p class="mt-4">Main Campus : 207 Imam Bonjol Street || <i class="mdi mdi-phone" title="phone"></i> (+6224) 3517261 || Other Campus : 5-11 Nakula I Street || <i class="mdi mdi-phone" title="phone"></i>(+6224) 3520165 || <i class="mdi mdi-printer" title="Fax"></i>Fax. (+6224) 3569684 || Semarang 50131 Indonesia || <i class="mdi mdi-mail" title="email"></i> <a href="https://dinus.ac.id/hospitality">Contact Us</a></p>
                        <ul class="list-unstyled social-icon social mb-0 mt-4">
                            <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i class="mdi mdi-facebook" title="Facebook"></i></a></li>
                            <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i class="mdi mdi-instagram" title="Instagram"></i></a></li>
                            <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i class="mdi mdi-twitter" title="Twitter"></i></a></li>
                        </ul><!--end icon-->
                    </div><!--end col-->
                    
                    <div class="col-lg-2 col-md-4 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                        <h4 class="text-light footer-head">Company</h4>
                        <ul class="list-unstyled footer-list mt-4">
                            <li><a href="#" class="text-foot"><i class="mdi mdi-chevron-right mr-1"></i> About us</a></li>
                            <li><a href="#" class="text-foot"><i class="mdi mdi-chevron-right mr-1"></i> Services</a></li>
                            <li><a href="#" class="text-foot"><i class="mdi mdi-chevron-right mr-1"></i> Team</a></li>
                            <li><a href="#" class="text-foot"><i class="mdi mdi-chevron-right mr-1"></i> Pricing</a></li>
                            <li><a href="#" class="text-foot"><i class="mdi mdi-chevron-right mr-1"></i> Features</a></li>
                            <li><a href="#" class="text-foot"><i class="mdi mdi-chevron-right mr-1"></i> FAQ</a></li>
                            <li><a href="#" class="text-foot"><i class="mdi mdi-chevron-right mr-1"></i> Blog</a></li>
                            <li><a href="#" class="text-foot"><i class="mdi mdi-chevron-right mr-1"></i> Login</a></li>
                        </ul>
                    </div><!--end col-->

                    <div class="col-lg-3 col-md-4 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                        <h4 class="text-light footer-head">Newsletter</h4>
                        <p class="mt-4">Sign up and receive the latest tips via email.</p>
                        <form>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="foot-subscribe form-group position-relative">
                                        <label>Write your email <span class="text-danger">*</span></label>
                                        <i class="mdi mdi-email ml-3 icons"></i>
                                        <input type="email" name="email" id="emailsubscribe" class="form-control pl-5 rounded" placeholder="Your email : " required>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <input type="submit" id="submitsubscribe" name="send" class="btn btn-primary w-100" value="Subscribe">
                                </div>
                            </div>
                        </form>
                    </div><!--end col-->
                </div><!--end row-->
            </div><!--end container-->
        </footer><!--end footer-->
        <hr>
        <footer class="footer footer-bar">
            <div class="container text-center">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="text-sm-left">
                            <p class="mb-0">© 2019 PSI UDINUS. UNIVERSITAS DIAN NUSWANTORO <i class="mdi mdi-heart text-danger"></i></p>
                        </div>
                    </div>

                    <div class="col-sm-6 mt-4 mt-sm-0 pt-2 pt-sm-0">
                    </div>
                </div>
            </div><!--end container-->
        </footer><!--end footer-->
        <!-- Footer End -->

        <!-- Back to top -->
        <a href="#" class="back-to-top rounded text-center" id="back-to-top"> 
            <i class="mdi mdi-chevron-up d-block"> </i> 
        </a>
        <!-- Back to top -->