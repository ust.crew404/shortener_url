-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 01 Okt 2019 pada 12.45
-- Versi server: 10.1.38-MariaDB
-- Versi PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shortener_url`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `short_url`
--

CREATE TABLE `short_url` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `url_asli` text NOT NULL,
  `url_short` text NOT NULL,
  `slug` varchar(10) NOT NULL,
  `description` text NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `short_url`
--

INSERT INTO `short_url` (`id`, `username`, `url_asli`, `url_short`, `slug`, `description`, `status`) VALUES
(1, '0686.89.2017.0005', 'https://dinus.ac.id/getinfo/3725/Jadwal-Pertemuan-Seminar-Proposal', 'http://dinus.id/B6rew', '-', 'Seminar Proposal SI-S1', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `statistic`
--

CREATE TABLE `statistic` (
  `id` int(11) NOT NULL,
  `url` varchar(50) NOT NULL,
  `ip` varchar(50) NOT NULL,
  `datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `statistic`
--

INSERT INTO `statistic` (`id`, `url`, `ip`, `datetime`) VALUES
(1, 'http://dinus.id/B6rew', '::1', '2019-10-01 12:35:11'),
(2, 'http://dinus.id/B6rew', '::1', '2019-10-01 12:36:55');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_user`
--

CREATE TABLE `tbl_user` (
  `tbl_id` int(11) NOT NULL,
  `npp` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `level` int(11) NOT NULL,
  `last_login` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_user`
--

INSERT INTO `tbl_user` (`tbl_id`, `npp`, `password`, `level`, `last_login`) VALUES
(1, '0686.89.2017.0005', '21232f297a57a5a743894a0e4a801fc3', 1, '2019-10-01 10:37:39');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `short_url`
--
ALTER TABLE `short_url`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `statistic`
--
ALTER TABLE `statistic`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`tbl_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `short_url`
--
ALTER TABLE `short_url`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `statistic`
--
ALTER TABLE `statistic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `tbl_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
